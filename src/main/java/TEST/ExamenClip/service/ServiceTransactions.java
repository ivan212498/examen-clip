package TEST.ExamenClip.service;

import TEST.ExamenClip.Models.Transaction;

public interface ServiceTransactions {

	public void add(String user_id, Transaction transaction) throws Exception;
	
	public void list(String user_id) throws Exception;
	
	public void show(String user_id, String transaction) throws Exception;
	
	public void sum(String user_id) throws Exception;
}
