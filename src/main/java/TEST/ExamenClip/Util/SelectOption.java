package TEST.ExamenClip.Util;

import com.google.gson.Gson;

import TEST.ExamenClip.Models.Transaction;
import TEST.ExamenClip.service.ServiceTransactions;

public class SelectOption {
	
	String[] args;
	ServiceTransactions servTr;
	
	public SelectOption(String[] args,ServiceTransactions servTr) {
		this.args = args;
		this.servTr = servTr;
	}
	
	public void Selector() {
		System.out.println(args[1]);
		switch(args[1]) {
			case "add":
				
				 AddTransaction(args[0], args[2]);
				break;
			case "sum":
				 SumAllTransactions(args[0]);
				break;
			case "list":
				 ListAllTransactions(args[0]);
				break;
			default:
				if(ValidateTransaction(args[1]))
					ShowTransaction(args[0],args[1]);
				break;
		}
		
		
		
	}

	private void ListAllTransactions(String user_id) {
		try {
			servTr.list(user_id);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void ShowTransaction(String user_id, String transactionUUID) {
		try {
			servTr.show(user_id, transactionUUID);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	private void SumAllTransactions(String user_id) {
		try {
			servTr.sum(user_id);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	private void AddTransaction(String user_id, String json) {
		try {
			Transaction transaction = new Gson().fromJson(json, Transaction.class);
			servTr.add(user_id, transaction);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			// TODO: handle exception
		}
	}

	public Boolean ValidateTransaction(String str) {
		String matchUUID_rgx = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";
		return str.matches(matchUUID_rgx);
	}
	
	
	
}
