package TEST.ExamenClip;

import java.util.regex.Pattern;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import TEST.ExamenClip.Util.SelectOption;
import TEST.ExamenClip.service.ServiceTransactions;

/**
 * Hello world!
 *
 */
public class App 
{
    private static ApplicationContext appContext;

	public static void main( String[] args )
    {
		
        if ( !args[0].matches("[0-9]+") || args.length < 2)
        	System.out.println("We need more than 1 parameters to operate");
        
        else {
	        appContext = new ClassPathXmlApplicationContext("TEST/ExamenClip/xml/beans.xml");
	        
	        ServiceTransactions servTr = (ServiceTransactions) appContext.getBean("serviceTransactionsImp");    
	        
	        try {
	    		SelectOption so = new SelectOption(args, servTr);
	    		so.Selector();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
        }
        
        
    }
}
