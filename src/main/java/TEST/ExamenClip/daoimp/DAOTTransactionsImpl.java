package TEST.ExamenClip.daoimp;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;

import TEST.ExamenClip.Models.Sum;
import TEST.ExamenClip.Models.Transaction;
import TEST.ExamenClip.Models.TransactionBD;
import TEST.ExamenClip.Util.TreatRS;
import TEST.ExamenClip.beans.Transactions;
import TEST.ExamenClip.dao.DAOTransactions;

@Repository
public class DAOTTransactionsImpl implements DAOTransactions {

	@Autowired
	private DataSource ds;
	
	public void add(String user_id, Transaction transaction) throws Exception {
		
		String sql = "INSERT INTO transactions(transaction_id, amount, description, date, user_id) VALUES (?,?,?,?,?)";
		Connection con = null;
		
		UUID idOne = UUID.randomUUID();
		
		try {
			con = ds.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);			
			
			ps.setString(1, idOne.toString());
			ps.setDouble(2, transaction.getAmount());
			ps.setString(3, transaction.getDescription());
			ps.setDate(4, transaction.getDate());
			ps.setInt(6, transaction.getUser_id());
			
			ps.executeUpdate();
			ps.close(); 
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally{
			if(con != null)
				con.close();
			
			show(transaction.getUser_id()+"", idOne+"");
			
		}
		
	}

	public void list(String user_id) throws Exception {
		String sql = "SELECT * FROM transactions WHERE user_id ="+user_id;
		Connection con = null;
				
		try {
			con = ds.getConnection();
			Statement st = con.createStatement();
            ResultSet resultSet = st.executeQuery(sql);
 
            TreatRS tr = new TreatRS();
            List<Map<String, Object>> lm =  tr.resultSetToList(resultSet);
            
            String json = new Gson().toJson(lm);
            
            System.out.println(json);
			
		} catch (Exception e) {
			// TODO: handle exception
		} finally{
			if(con != null)
				con.close();
		}
		
	}

	public void show(String user_id, String transaction_id) throws Exception {

		String sql = "SELECT * FROM transaction WHERE transaction_id = "+transaction_id+" user_id = "+user_id;
		Connection con = null;
				
		try {
			con = ds.getConnection();
			Statement st = con.createStatement();
            ResultSet resultSet = st.executeQuery(sql);
            

            TreatRS trrs = new TreatRS();
            List<Map<String, Object>> lm =  trrs.resultSetToList(resultSet);
            
            TransactionBD tr = new TransactionBD();
            
            tr.setAmount((Double)lm.get(0).get("amount"));
            tr.setDate((Date)lm.get(0).get("date"));
            tr.setDescription((String)lm.get(0).get("description"));
            tr.setTransaction_id((String)lm.get(0).get("transaction_id"));
            tr.setUser_id((Integer)lm.get(0).get("user_id"));
            
            
            String json = new Gson().toJson(tr, Transaction.class);
            
            System.out.println(json);
            
            
		} catch (Exception e) {
			// TODO: handle exception
		} finally{
			if(con != null)
				con.close();
		}
		
		
	}

	public void sum(String user_id) throws Exception {
		
		String sql = "SELECT user_id, SUM(amount) AS sum FROM transactions WHERE user_id = "+user_id;
		Connection con = null;
				
		try {
			con = ds.getConnection();
			Statement st = con.createStatement();
            ResultSet resultSet = st.executeQuery(sql);
            
            TreatRS tr = new TreatRS();
            List<Map<String, Object>> lm =  tr.resultSetToList(resultSet);
            
            Sum sum = new Sum();
            sum.setSum((Double)lm.get(0).get("sum"));
            sum.setUser_id((Integer)lm.get(0).get("user_id"));
            
            String json = new Gson().toJson(sum, Sum.class);
            
            System.out.println(json);
            
		} catch (Exception e) {
			// TODO: handle exception
		} finally{
			if(con != null)
				con.close();
		}
	}

	
	

}
