package TEST.ExamenClip.serviceimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import TEST.ExamenClip.Models.Transaction;
import TEST.ExamenClip.beans.Transactions;
import TEST.ExamenClip.dao.DAOTransactions;
import TEST.ExamenClip.service.ServiceTransactions;

@Service
public class ServiceTransactionsImp implements ServiceTransactions {
	
	@Autowired
	private DAOTransactions daoTrransactions;

	public void add(String user_id, Transaction transaction) throws Exception {
		try {
			
			daoTrransactions.add(user_id, transaction);
			
		} catch (Exception e) {
			// TODO: handle exception
		} 
		
	}

	@Override
	public void list(String user_id) throws Exception {
		try {
			
			daoTrransactions.list(user_id);
			
		} catch (Exception e) {
			// TODO: handle exception
		} 
		
	}

	@Override
	public void show(String user_id, String transaction) throws Exception {
		try {
			
			daoTrransactions.show(user_id, transaction);
			
		} catch (Exception e) {
			// TODO: handle exception
		} 
	}

	@Override
	public void sum(String user_id) throws Exception {
		try {
			
			daoTrransactions.sum(user_id);
			
		} catch (Exception e) {
			// TODO: handle exception
		} 
		
	}

	
	
	

}
